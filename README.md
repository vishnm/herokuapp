These are just a few example implementations for some features on the Heroku Internet Page using Watir-Webdriver, Cucumber and Test Unit.

Pre-requisites
Ruby 2.2.0 and bundler
I recommend using RVM for Ruby-Installations.
Install Ruby running rvm install 2.2.0.
Install Bundler running gem install bundler or if you want to have the bundler available at all time, run rvm @global do gem install bundler.
Download a stable or the newest Selenium Server. You can start the server running java -jar selenium-server-standalone-<your_version_here>.jar
Clone/download the project to the folder of your choice.
Cd into the root folder and run bundle install to install Cucumber and Watir-Webdriver
Run test unit to execute the tests.