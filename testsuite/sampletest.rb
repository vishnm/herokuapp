require 'HeroKuAPP'

herokuapp = HeroKuAPP.new()

results = []
testResult = ""

herokuapp.loginpage.type_username("test")
herokuapp.loginpage.type_password("SuperSecretPassword!")
herokuapp.loginpage.click_login
errorMsg = herokuapp.check_for_text("Your username is invalid")
if (errorMsg)
   results << "PASS"
else
   results << "FAIL"
end 

herokuapp.loginpage.type_username("tomsmith")
herokuapp.loginpage.type_password("test")
herokuapp.loginpage.click_login
errorMsg = herokuapp.check_for_text("Your password is invalid")
if (errorMsg)
   results << "PASS"
else
   results << "FAIL"
end 

herokuapp.loginpage.type_username("tomsmith")
herokuapp.loginpage.type_password("SuperSecretPassword!")
herokuapp.loginpage.click_login
errorMsg = herokuapp.check_for_text("Your logged into a secured area")
if (errorMsg)
   results << "PASS"
else
   results << "FAIL"
end

herokuapp.logout

unless (results.include? "FAIL")
    testResult = "PASS"
else
    testResult = "FAIL"
end
