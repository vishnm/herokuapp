require 'rubygems'
gem 'test-unit'
require 'test-unit'
require 'test/unit/runner/html'

class TestExample < Test::Unit::TestCase
  def setup
    puts "running the setup........."
    @number1 = 4
    @number2 = 2    
  end

  def run_all_scripts
     tcResults = []
    @startTime = Time.now
      @before_run.call
      if @protocolList.empty?
         msg = "Fatal Error\nNo scripts found"
         @pLP.write_to_log_and_console(msg)
         raise "No scripts found to run, aborting"
      end
      @protocolList.each do |protocol|
         begin
            fileName = protocol["path"].basename.sub_ext('').to_s
            @scriptName = fileName
            @mainFeature = protocol["path"].to_s.gsub(".rb", ".xml")
            runThis = protocol["path"]
            log_run_begin(fileName)
            scriptStartTime = Time.now
            @before_script.call
            scriptEndTime = Time.now
            @pLP.build_html_result(protocol["id"], tcResults)  #-- BUILDS HTML RESULT
            @pLP.update_overall_summary(protocol["id"], tcResults)
            @scriptResult = tcResults.overallResult
            @after_script.call
            @scriptName = nil
            @scriptResult = nil
         rescue
            @crashCount += 1
            @pLP.write_to_log_and_console("Error while running #{fileName}: #{$!}")
         end
      end
      @after_run.call
  end

  def teardown
    puts "running teardown......."
    @number = nil
  end  
  
end	