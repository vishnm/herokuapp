#========================================================================================
# File Name: LoginPage.rb
#
#========================================================================================
$:.unshift File.join(File.dirname(__FILE__))
require 'HeroKuAPP.rb'

#========================================================================================
# CLASS
#     LoginPage
#
# BASE CLASS
#     HeroKuAPP
#========================================================================================

class LoginPage < HeroKuAPP
   page_url = @browser.goto "http://the-internet.herokuapp.com/login"
   
   #=====================================================================================
   # NAME
   #     LoginPage::type_username
   #
   # DESCRIPTION
   #     Enters username on Login Page
   #
   # PARAMETERS
   #     userName = user name for mednet
   #
   # RETURN VALUE
   #     true upon success; nil and error string upon failure
   #=====================================================================================
   def type_username(userName)
      return set_value(@username, userName)
   end

   #=====================================================================================
   # NAME
   #     LoginPage::get_username
   #
   # DESCRIPTION
   #     Gets the username on Login Page
   #
   # PARAMETERS
   #     None
   #
   # RETURN VALUE
   #     username if found, else nil
   #=====================================================================================
   def get_username
      return get_value(@username)
   end

   #=====================================================================================
   # NAME
   #     LoginPage::type_password
   #
   # DESCRIPTION
   #     Enters password on Login Page
   #
   # PARAMETERS
   #     password = password for mednet
   #
   # RETURN VALUE
   #     true upon success; nil and error string upon failure
   #=====================================================================================
   def type_password(password)
      return set_value(@password, password)
   end

   #=====================================================================================
   # NAME
   #     LoginPage::get_password
   #
   # DESCRIPTION
   #     Gets the password on Login Page
   #
   # PARAMETERS
   #     None
   #
   # RETURN VALUE
   #     password if found, else nil
   #=====================================================================================
   def get_password
      return get_value(@password)
   end

   #=====================================================================================
   # NAME
   #     LoginPage::click_login
   #
   # DESCRIPTION
   #     Clicks login button
   #
   # PARAMETERS
   #     nextPage -- page to make active after logging in (usually "HomePage", but this
   #                 can change based on the "My Account" settings for the user)
   #
   # RETURN VALUE
   #     true upon success; nil and error string upon failure
   #=====================================================================================
   def click_login
      retVal = click(@login)
      if (retVal)
         if (check_for_text(invalidLogin))
            retVal = handle_error(invalidLogin)
         elsif (check_for_text(userNameEmpty) and check_for_text(passwordEmpty))
            retVal = handle_error(userNameEmpty + " And " + passwordEmpty)
         elsif (check_for_text(userNameEmpty))
            retVal = handle_error(userNameEmpty)
         elsif (check_for_text(passwordEmpty))
            retVal = handle_error(passwordEmpty)
         elsif (check_for_text(nonPrintableChr))
            retVal = handle_error(nonPrintableChr)
         elsif (check_for_text(passwordNonPrintableChr))
            retVal = handle_error(passwordNonPrintableChr)
         elsif (check_for_text(spaceChr))
            retVal = handle_error(spaceChr)    
         end
      end
      return retVal
   end
end   

