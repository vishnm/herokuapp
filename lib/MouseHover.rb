#========================================================================================
# File Name: MouseHover.rb
#========================================================================================
$:.unshift File.join(File.dirname(__FILE__))
require 'HeroKuAPP.rb'

#========================================================================================
# CLASS
#     MouseHover
#
# BASE CLASS
#     HeroKuAPP
#========================================================================================
class MouseHover < HeroKuAPP
   page_url = @browser.goto "http://the-internet.herokuapp.com/hovers"
   
   #=====================================================================================
   # NAME
   #     MouseHover::mouser_hover_image
   #
   # PARAMETERS
   #     id - Control id which is in the HTML page
   #
   #===================================================================================== 
   def mouser_hover_image (id, waitTime = 2)     
      retVal = nil   
      autoIt = WIN32OLE.new("AutoItX3.Control")
      begin
         midx, midy = get_coordinates(id, true)                  
         @browser.rautomation.mouse.move({:x => midx, :y => midy})
         sleep 1
         retVal = @browser.rautomation.mouse.move
         retVal = true
         sleep 1         
      end
      return retVal
   end
   
   #=====================================================================================
   # NAME
   #     MouseHover::get_coordinates
   #
   # PARAMETERS
   #     id - Control id which is in the HTML page
   #     mid = false by default. If true then returns the mid center coordinates x & y
   #
   # RETURN VALUE
   #     if mid = false returns left, right, top, bottom coordinates or
   #     mid = true returns mid value x and y coordnidates if success; nil and 
   #     outputs error string on failure
   #=====================================================================================   
   def get_coordinates(id, mid = false)
      retVal = nil
      begin
         doc = @browser.document.getElementById(id)
         doc.focus
         left = doc.getBoundingClientRect.left
         right = doc.getBoundingClientRect.right    
         parentWindowHeight = @browser.document.parentWindow.screenTop      
         top = doc.getBoundingClientRect.top + parentWindowHeight
         bottom = doc.getBoundingClientRect.bottom + parentWindowHeight         
         if mid.eql? true
            midx = (left + right) / 2
            midy = ((top + bottom) / 2) 
            retVal = midx, midy
         else
            retVal = left , right, top, bottom
         end   
      rescue
         retVal = handle_error("Unable to click element with id :#{id}")
      end  
      return retVal
   end
   
end   