#========================================================================================
# File Name: SortableHeaders.rb
#========================================================================================
$:.unshift File.join(File.dirname(__FILE__))
require 'HeroKuAPP.rb'

#========================================================================================
# CLASS
#     SortableHeaders
#
# BASE CLASS
#     HeroKuAPP
#========================================================================================
class SortableHeaders < HeroKuAPP
   page_url = @browser.goto "http://the-internet.herokuapp.com/tables"
   
   #=====================================================================================
   # NAME
   #     SortableHeaders::click_sortable_header
   #
   # DESCRIPTION
   #     Clicks on a sortable header.
   #
   # PARAMETERS
   #     name - A header name
   #
   # RETURN VALUE
   #     true : successful; nil & outputs error string otherwise.
   #=====================================================================================
   def click_sortable_header(name, sortOrder = nil)
      headerList = get_sortable_headers     
      titles = get_sortable_header_titles        
      retVal = nil
      unless ((headerList.nil?) || (titles.nil?))
         if (titles.include? name)
            headerIndex = titles.index(name)                      
            control = @headerList + headerIndex.to_s            
            unless (control.nil?)
               @b.div(:id => control).click 
               retVal = true               
            else
               retVal = ("Unable to find the header #{name} on the active page")
            end
         else
            retVal = handle_error("The header #{name} is not a sortable Header.")
         end
      else
         retVal = handle_error("Unable to retrieve the sortable headers on the active page.")
      end
      return retVal
   end
   
   #=====================================================================================
   # NAME
   #     SortableHeaders::get_sortable_headers
   #
   # PARAMETERS
   #     tableName - Table name where the header is located.
   #
   # RETURN VALUE
   #     An array of sortable headers: successful; nil & outputs error
   #     string otherwise.
   #=====================================================================================
   def get_sortable_headers(tableName = 'Example 2')
      attributeXml = get_header_labels_attributes_node #get_header_labels_attributes_node gets the page element properties of "http://the-internet.herokuapp.com/tables"      
      container = get_container_table(tableName) unless (tableName.nil?) #get_container_table from "http://the-internet.herokuapp.com/tables" 
      begin
         controls = nil
         controls = (container.nil?)? get_controls(attributeXml) : get_controls(attributeXml, true, container)
      rescue
         puts "Error: [Incorrect Table name] No table of name:#{tableName} found"
      end      
      return controls
   end
   
   #=====================================================================================
   # NAME
   #     SortableHeaders::get_sortable_header_titles
   #
   # PARAMETERS
   #     tableName - Table name where the header is located.
   #
   # RETURN VALUE
   #     A array of sortable headers as strings: successful; 
   #     nil & outputs error string otherwise.
   #=====================================================================================
   def get_sortable_header_titles(tableName = 'Example 2')
      attributeXml = get_header_labels_attributes_node #get_header_labels_attributes_node gets the page element properties of "http://the-internet.herokuapp.com/tables"      
      headList = []
      container = get_container_table(tableName) unless (tableName.nil?)
      begin      
         headList = (container.nil?)? get_controls(attributeXml) : get_controls(attributeXml, true, container)
      rescue
         puts "Error: [Incorrect Table name] No table of name:#{tableName} found"
      end
      return headList
   end
   
end   


 