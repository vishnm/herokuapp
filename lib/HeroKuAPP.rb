#========================================================================================
# File Name: HeroKuPP.rb
#
#========================================================================================
$:.unshift File.join(File.dirname(__FILE__))

require 'nokogiri'
require 'pathname'
require 'win32ole'
require 'watir-webdriver'

#--------------------------------
# Overriding the "method_missing"
Watir::IE
module Watir
  class Element
     def method_missing(method_name, *args, &block)
        meth = method_name.to_s
        if meth =~ /(.*)_no_wait/ && self.respond_to?($1)
           perform_action do
              ruby_code = generate_ruby_code(self, $1, *args)
              ruby_code = ruby_code.chomp.split('.unshift').uniq.join('.unshift')
              system(spawned_no_wait_command(ruby_code))
           end
        elsif meth =~ /^data_(.*)/
           self.send(:attribute_value, meth.gsub("_", "-")) || ''
        else
           super
        end
     end
  end
end

class HeroKuAPP
  
   #-------------------------------------------------------------------------------------
   # CONSTRUCTION
   #-------------------------------------------------------------------------------------
   def initialize(ip = nil, userName = nil, password = nil)
      curDirectory = Dir.pwd
      Dir.chdir(File.dirname(__FILE__))
      pathName = Pathname.new("../Configuration/HeroKuAPP.xml")
      @configFilePath = pathName.realpath
      @isClosed = false
      Dir.chdir(curDirectory)
      
      begin
         WIN32OLE.ole_initialize
         if (PageFactory.useWebdriver.eql? true)          
            if (@defaultBrowser.eql? 'firefox')
               @browser = Watir::Browser.new(:firefox)
            else
               @browser = Watir::Browser.new(:ie)       
            end
            sleep(1) #-- Give browser time to open before trying to maximize
            width = @browser.execute_script("return screen.width;")
            height = @browser.execute_script("return screen.height;") 
            @browser.window.move_to(0,0) 
            @browser.window.resize_to(width, height)
         else
            @browser = Watir::Browser.new
            sleep(1)
            @browser.maximize
            @browser.activate #-- Bring the browser to foreground to avoid blank screen appearing in evidence
         end
      rescue
         puts "Unable to create or maximize new browser object: Exception: #{$!}"
      end
      
   end
   
   #=====================================================================================
   # NAME
   #     HeroKuAPP::set_value
   #
   # DESCRIPTION
   #     Fills in a text box found by id
   #
   # PARAMETERS
   #     old way: to be deprecated
   #     watirFunc - the specific Watir function to use
   #     attribute - the particular attribute given such href or id
   #     value     - the value of the attribute to use
   #     str       - string representing text to type in box
   #     
   #     new way:
   #     xmlNode - xml to extract watir parameters from
   #     str     - string representing text to type in box
   #
   # RETURN VALUE
   #     true if success; nil and outputs error string on failure
   #=====================================================================================
   def set_value(*args)
      watirFunc = nil
      attribute = nil
      value     = nil
      retVal    = nil
      str       = ""
      if (args.size == 2) #-- method overloaded (new way)
         params = ConfigManager.get_node_values(args[@xmlParamIndex])
         unless (params.eql? nil)
            watirFunc = params[@methodIndex]
            attribute = params[@attributeIndex]
            value     = params[@valueIndex]
            str       = args[@paramIndex]
         end
      else #-- method old way with 4 arguments (this is deprecated and will be removed)
         watirFunc = args[@methodIndex] 
         attribute = args[@attributeIndex] 
         value     = args[@valueIndex]
         str       = args[@nextPageIndex]
      end

      begin
         control = get_control(watirFunc, attribute, value)
         if (control.enabled?)
            if (control.respond_to? :readonly?)
               control.set(str) if ((control.readonly?).eql? false)
               retVal = true
            else
               control.set(str) unless (str.nil?)
               retVal = true
            end
         else
            retVal = puts "Element with value: #{value} is not enabled or is readonly!"
         end
      rescue
         errStr = puts "Unable to write >#{str}< in textbox with id #{value}; Exception: #{$!}"
         retVal = errStr
      end
      return retVal
   end
   
   #=====================================================================================
   # NAME
   #     HeroKuAPP::check_for_text
   #
   # DESCRIPTION
   #     Checks for text in the browser
   #
   # PARAMETERS
   #     text - text to look for
   #
   # RETURN VALUE
   #     true if found; false if not; nil and error string upon exception
   #=====================================================================================
   def check_for_text(text)
      retVal = false
      begin
         retVal = @browser.text.include?(text)
      rescue
         retVal = puts "Unable to check for text in the browser - Exception: #{$!}"
      end
      return retVal
   end
  
   #=====================================================================================
   # NAME
   #     HeroKuAPP::click
   #
   # DESCRIPTION
   #     Clicks based on the given watir function and attributes
   #
   # PARAMETERS
   #     Old Way: to be deprecated
   #     watirFunc - the specific Watir function to use
   #     attribute - the particular attribute given such href or id
   #     value     - the value of the attribute to use
   #     nextPage  - name of next page which will open after clicking on this control,
   #                    nil if new page won't open on click (optional, default=nil)
   #                    
   #     New Way:
   #     xmlNode - xml to extract watir parameters from   
   #     nextPage  - name of next page which will open after clicking on this control,
   #                    nil if new page won't open on click (optional, default=nil)          
   #
   # RETURN VALUE
   #     true upon success; nil and error string upon failure
   #=====================================================================================
   def click(xmlNode = nil, nextPage = nil)
      retVal    = nil
      @clickedButtonText = ""      
      params = ConfigManager.get_node_values(xmlNode)
      unless (params.eql? nil)
         watirFunc = params[@methodIndex]
         attribute = params[@attributeIndex]
         value     = params[@valueIndex]
      end
      @nextPage = nextPage
      begin
         control = get_control(watirFunc, attribute, value)

         #-----------------------------------------------------------------------------
         # Check is element is enabled. This will raise an exception if control wasn't
         # found which will be handled in the rescue.  If the control exists but is not
         # active the else will record an error and return nil
         if (control != nil)
            @clickedButtonText = control.value if (control.respond_to?(:value))           
            control.focus
            control.when_present.click
            
            #-----------------------------------------
            # set pageOpen to nextPage which is opened
            # on click if nextPage is not nil
            retVal = true            
         end
      rescue
         retVal = puts "Unable to click element with value: #{value}; Exception: #{$!}"
      end
      return retVal
   end
   
   #=====================================================================================
   # NAME
   #     HeroKuAPP::handle_error
   #
   # DESCRIPTION
   #     Handles error for class methods by building error string and return nil value
   #
   # PARAMETERS
   #     string - error string for calling method
   #
   # RETURN VALUE
   #     nil
   #=====================================================================================
   def handle_error(string)
      caller[0]=~ #{/`(.*?)'/}  # note the first quote is a back-tick
      methodName = $1
      
      srcInfoArray = caller[0].split(':')
      
      pathname = srcInfoArray[0] + ":" + srcInfoArray[1]
      srcline  = srcInfoArray[2]
      
      #----------------------------------------------
      #self - the 'self' object of the calling method
      @lastErrorStr  = "ERROR :  #{string}\n"
      @lastErrorStr += "FILE  :  #{pathname}; line #{srcline}\n"
      @lastErrorStr += "METHOD:  #{self.class}::#{methodName}"
      
      #----------------------------------------------
      #self - the 'self' object of the calling method
      string.gsub!(/\n/, " ")
      methodStr = "#{self.class}::#{methodName}"
      
      #-----------------
      # Write error info
            
      return nil
   end
   
   #=====================================================================================
   # NAME
   #     HeroKuAPP::logout
   #
   # PARAMETERS
   #     None
   #
   # RETURN VALUE
   #     true upon success; false if unable; nil and error string upon exception raised
   #=====================================================================================
   def logout
      retVal = false
      begin
         @browser.link(:text, 'Logout').focus
         @browser.link(:text, 'Logout').when_present.click
         sleep (2)
         retVal = (@browser.title.eql? @logoutTitle)? true : false
      rescue
         retVal = handle_error("Unable to find logout control!; Exception: #{$!}")
      end
      return retVal
   end
   
end